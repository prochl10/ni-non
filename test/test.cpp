//
// Created by lucie.prochazkova on 05.12.2022.
//
#include "../src/DenseMatrix.h"
#include "../src/Solver.h"
#include "../src/SparseMatrix.h"

#include <unistd.h>
#include <fstream>
#include <cassert>

using namespace std;

DenseMatrix * loadDenseMatrix(string & s) {
    ifstream f(s);
    size_t n;
    if(!f.is_open()) {
        throw invalid_argument("Bad file" + s);
    }
    f >> n;
    vector<vector<double>> v;
    v.resize(n);
    for(size_t i = 0; i < n; ++i) {
        v[i].resize(n, 0);
        for(size_t j = 0; j < n; ++j) {
            f >> v[i][j];
        }
    }
    return new DenseMatrix(v, n);
}


std::vector<double> loadVector(string & s){
    ifstream f(s);
    if(!f.is_open()) {
        throw invalid_argument("Bad file ");
    }
    std::vector<double> v;
    int n;
    f >> n;
    double x;
    for(int j = 0; j < n; ++j) {
        f >> x;
        v.push_back(x);
    }
    return v;
}


SparseMatrix * loadSparseMatrix(string s) {
    size_t n, elements;
    ifstream f(s);

    if(!f.is_open()) {
        throw invalid_argument("Bad file" + s);
    }

    f >> n >> elements;
    vector<uint> row;
    vector<uint> col;
    vector<double> values;
    int r,c, cr = -1;
    double v;
    for(size_t i = 0; i < elements; ++i) {
        f >> r >> c >> v;
        if(r > cr){
            while(cr < r) {
                row.push_back(values.size());
                ++cr;
            }
        }
        col.push_back(c);
        values.push_back(v);
    }
    return new SparseMatrix(n, values, col, row);
}


int main() {
    string testfile = "test01";
    DenseMatrix * m = loadDenseMatrix(testfile);
    assert(m->get(1,1) == 1);
    assert(m->get(0,1) == 0);
    assert(m->get(0,0) == 0);
    testfile = "testvec.txt";
    auto v = loadVector(testfile);
    Solver solver;
    auto f = m->multiply(v);
    vector<double> r({3,6,9});
    assert(f == r);
    assert(solver.multiply(1,v) == v);
    assert(solver.multiply(v,v) == 14);
    assert(solver.multiply(r,v) == 42);
    assert(solver.multiply(r,r) == 126);

    return 0;
}