//
// Created by lucie.prochazkova on 04.12.2022.
//

#include "DenseMatrix.h"
#include <stdexcept>

using namespace std;

vector<double> DenseMatrix::multiply(vector<double> & vec) {
    if (vec.size() != size) {
        throw invalid_argument("Multiplication error");
    }
    vector<double> res;
    for (size_t i =  0; i < size; ++i) {
        res.push_back(0);
        for (size_t j =  0; j < size; ++j) {
            res[i] += get(i,j) * vec[j];
        }
    }
    return res;
}


double DenseMatrix::get(size_t i, size_t j) {
    if (i >= size || j >= size) {
        throw invalid_argument("Index out of bounds, " + to_string(i) + ", " + to_string(j) + " not in "
                               + to_string(size) + " sized matrix");
    }
    return values[i][j];
}

void DenseMatrix::multiply(vector<double> &vec, vector<double> &res) {
    if (vec.size() != size || res.size() != size) {
        throw invalid_argument("Multiplication error");
    }
    for (size_t i =  0; i < size; ++i) {
        res[i] = 0;
        for (size_t j =  0; j < size; ++j) {
            res[i] += get(i,j) * vec[j];
        }
    }
}
