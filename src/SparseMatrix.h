//
// Created by lucie.prochazkova on 04.12.2022.
//

#ifndef NI_NON_SPARSEMATRIX_H
#define NI_NON_SPARSEMATRIX_H
#include <vector>
#include "Matrix.h"

class SparseMatrix: public Matrix {
    std::vector<double> val;
    std::vector<unsigned> col_ind, row_ptr;

public:
    SparseMatrix(size_t n, std::vector<double> & v, std::vector<unsigned> &c, std::vector<unsigned> & r): Matrix(n), val(v), col_ind(c), row_ptr(r) {row_ptr.push_back(size);};
    double get(size_t i, size_t j) override;
    std::vector<double> multiply(std::vector<double> & vec) override;

    void multiply(std::vector<double> &vec, std::vector<double> &res) override;
};


#endif //NI_NON_SPARSEMATRIX_H
