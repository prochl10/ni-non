//
// Created by lucie.prochazkova on 04.12.2022.
//

#ifndef NI_NON_MATRIX_H
#define NI_NON_MATRIX_H


class Matrix {
protected:
    size_t size;
public:
    virtual ~Matrix() = default;
    explicit Matrix(size_t n): size(n) {};
    virtual double get(size_t i, size_t j) = 0;
    virtual std::vector<double> multiply(std::vector<double> & vec) = 0;
    virtual void multiply(std::vector<double> & vec, std::vector<double> & res) = 0;

    int getSize() const {return size;}
};


#endif //NI_NON_MATRIX_H
