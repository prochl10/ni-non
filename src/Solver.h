//
// Created by lucie.prochazkova on 04.12.2022.
//

#ifndef NI_NON_SOLVER_H
#define NI_NON_SOLVER_H

#include <vector>
#include "Matrix.h"

enum Method {
    GRADIENT_DESCENT,
    CONJUGATE_GRADIENT
};

class Solver {
public:
    constexpr static double EPS =  1e-2;
    constexpr static size_t max_iter =  1e11;

    static double multiply(std::vector<double> & a, std::vector<double> & b);
    static std::vector<double> multiply( double a, std::vector<double> & b);



    static std::vector<double> solve(Method method, Matrix * matrix, std::vector<double> & vector);

    static std::vector<double> gradient_descent(Matrix * matrix, std::vector<double> & v);

    static std::vector<double> conjugate_gradient(Matrix * matrix, std::vector<double> & v);


    static std::vector<double> add(std::vector<double> &a, std::vector<double> &b);

    static double norm(std::vector<double> &a);
};


#endif //NI_NON_SOLVER_H
