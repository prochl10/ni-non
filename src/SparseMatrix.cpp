//
// Created by lucie.prochazkova on 04.12.2022.
//

#include "SparseMatrix.h"
#include <stdexcept>

using namespace std;

double SparseMatrix::get(size_t i, size_t j) {
    if (i >= size || j >= size) {
        throw invalid_argument("Index out of bounds, " + to_string(i) + ", " + to_string(j) + " not in "
                               + to_string(size) + " sized matrix");
    }
    for (size_t x = row_ptr[i]; x < row_ptr[i+1]; ++x) {
        if(col_ind[x]==j)
            return val[x];
    }
    return 0;
}

std::vector<double> SparseMatrix::multiply(std::vector<double> & vec) {
    if (vec.size() != size) {
        throw invalid_argument("Multiplication error");
    }
    vector<double> res;
    for (size_t i =  0; i < size; ++i) {
        res.push_back(0);

        for(size_t j = row_ptr[i]; j < row_ptr[i+1]; ++j){
            res[i] += vec[col_ind[j]]*val[j];
        }
    }
    return res;
}

void SparseMatrix::multiply(vector<double> &vec, vector<double> &res) {
    if (vec.size() != size || res.size() != size) {
        throw invalid_argument("Multiplication error");
    }
    for (size_t i =  0; i < size; ++i) {
        res[i] = 0;
        for(size_t j = row_ptr[i]; j < row_ptr[i+1]; ++j){
            res[i] += vec[col_ind[j]]*val[j];
        }
    }
}
