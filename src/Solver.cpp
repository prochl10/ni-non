//
// Created by lucie.prochazkova on 04.12.2022.
//

#include "Solver.h"
#include <stdexcept>
#include <iostream>

using namespace std;


//#define DEBUG

/**
 * Solves system of linear equations
 * @param method given method, can be either gradient (steepest) descent or conjugate gradient
 * @param matrix matrix
 * @param vector vector of constant term
 * @return Solution
 */
std::vector<double> Solver::solve(Method method, Matrix * matrix, std::vector<double> & vector) {

    switch (method) {
        case GRADIENT_DESCENT:
            return gradient_descent(matrix, vector);
        case CONJUGATE_GRADIENT:
            return conjugate_gradient(matrix, vector);
        default:
            return {};
    }
}


/**
 *  Solves system of linear equations using gradient descent method
 * @param matrix matrix to solve
 * @param v vector of constant terms
 * @return Solution
 *
 * Stops if norm of r vector is less than @link:Solver.EPS squared or iterations are more than @link:Solver.max_iter
 */
std::vector<double> Solver::gradient_descent(Matrix *matrix, std::vector<double> &v) {
    size_t iter = 0;
    vector<double> x_curr (matrix->getSize(), 0);
    vector<double> r_curr (v);
    double eps2 =  EPS * EPS;
    cout << "Start solving" << endl;
    double rr = multiply(r_curr, r_curr);
    double alpha_k;

    vector<double> Ark(matrix->getSize());
    while(42) {
        matrix->multiply(r_curr, Ark);
        alpha_k = rr / multiply(r_curr, Ark);
        rr = 0;
        for (size_t i = 0; i < x_curr.size(); ++i ) {
            x_curr[i] += alpha_k*r_curr[i];
            r_curr[i] -= alpha_k * Ark[i];
            rr += r_curr[i]*r_curr[i];
        }

        if(iter % 100 == 0) {
            cout << "Iteration " << iter << " ";
#ifdef DEBUG
            cout << norm(r_curr) << " " << eps2 << " ";
            auto res = matrix->multiply(x_curr);
            for (int i = 0; i < 2 ; ++i) {
                cout << "\t" << res[i]<< " " << v[i];

            }
#endif // DEBUG
            cout << endl;
        }

        if (rr <= eps2 || iter > max_iter ) { //matrix->getSize()
            break;
        }

        iter++;
    }
cout << "End iteration " << iter  << " rr " << rr << endl;
    return x_curr;
}

/**
 *  Solves system of linear equations using conjugate gradient method
 * @param matrix matrix to solve
 * @param v vector of constant terms
 * @return Solution
 *
 * Stops if norm of r vector is less than @link:Solver.EPS squared or iterations are more than @link:Solver.max_iter
 */
std::vector<double> Solver::conjugate_gradient(Matrix *matrix, vector<double> &v) {
    size_t iter = 0;
    vector<double> x_curr (matrix->getSize(), 0);
    vector<double> r_curr (v);
    vector<double> s_curr (v);

    cout << "Start solving" << endl;
    double rr = multiply(r_curr, r_curr);
    double rk;
    vector<double> Ask(matrix->getSize());
    while(42) {
        matrix->multiply(s_curr, Ask);
        double alpha_k = rr / multiply(s_curr, Ask);
        rk=rr;
        rr = 0;
        for (size_t i = 0; i < s_curr.size(); ++i) {
            x_curr[i] += alpha_k * s_curr[i];
            r_curr[i] -= alpha_k * Ask[i];
            rr += r_curr[i]*r_curr[i];
        }
        if(iter % 100 == 0) {
            cout << "Iteration " << iter << " ";
#ifdef DEBUG
            cout << norm(r_curr) << " " << EPS * EPS << " ";
#endif // DEBUG
            cout << endl;
        }

        if ( rr < EPS * EPS || max_iter < iter) {
            break;
        }

        double beta = rr/rk;
        for (size_t i = 0; i < s_curr.size(); ++i) {
            s_curr[i] = r_curr[i] + beta * s_curr[i];
        }
        iter++;
    }
    cout << "End iter"<< iter << " rr " << rr <<endl;
    return x_curr;
}

std::vector<double> Solver::multiply(double a, std::vector<double> & b) {

    std::vector<double> res(b);
    for (double & re : res)
        re *= a;

    return res;
}

std::vector<double> Solver::add(vector<double> &a, vector<double> &b) {
    if (a.size() != b.size()) {
        throw invalid_argument("Adding vectors with mismatched sizes");
    }
    std::vector<double> res(a);

    for (size_t i = 0; i < a.size(); ++i) {
        res[i] += b[i];
    }
    return res;
}

double Solver::norm(vector<double> &a) {
    double res = 0;
    for( double x :a ) {
        res += x*x;
    }
    return res;
}

double Solver::multiply(vector<double> &a, vector<double> &b) {

    if (a.size() != b.size()) {
        throw invalid_argument("Multiplying vectors with mismatched sizes");
    }
    double res = 0;

    for (size_t i = 0; i < a.size(); ++i) {
        res += a[i]*b[i];
    }
    return res;
}
