//
// Created by lucie.prochazkova on 04.12.2022.
//

#ifndef NI_NON_DENSEMATRIX_H
#define NI_NON_DENSEMATRIX_H
#include <vector>
#include "Matrix.h"


class DenseMatrix: public Matrix {
    std::vector<std::vector<double>> values;


public:
    DenseMatrix(std::vector<std::vector<double>> & v, size_t s):  Matrix(s), values(v) {};
    std::vector<double> multiply(std::vector<double> & vec) override;
    double get(size_t i, size_t j) override;

    void multiply(std::vector<double> &vec, std::vector<double> &res) override;
};


#endif //NI_NON_DENSEMATRIX_H
