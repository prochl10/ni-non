#include <iostream>
#include "Solver.h"
#include "DenseMatrix.h"
#include "SparseMatrix.h"
#include <unistd.h>
#include <fstream>

#define DEBUG

using namespace std;

DenseMatrix * loadDenseMatrix() {
    size_t n;
    cout << "Enter your matrix filename" << endl;
    string s;
    cin >> s;
    ifstream f(s);

    if(!f.is_open()) {
        throw invalid_argument("Bad file" + s);
    }
    f >> n;
    vector<vector<double>> v;
    v.resize(n);
    for(size_t i = 0; i < n; ++i) {
        v[i].resize(n, 0);
        for(size_t j = 0; j < n; ++j) {
            f >> v[i][j];
        }
    }
    return new DenseMatrix(v, n);
}

SparseMatrix * loadSparseMatrix() {
    size_t n, elements;
    cout << "Enter your matrix filename" << endl;
    string s;
    cin >> s;
    ifstream f(s);

    if(!f.is_open()) {
        throw invalid_argument("Bad file" + s);
    }

    f >> n >> elements;
    vector<uint> row;
    vector<uint> col;
    vector<double> values;
    int r,c, cr = -1;
    double v;
    cout << "n: " << n << " Elems " << elements << endl;
    for(size_t i = 0; i < elements; ++i) {
        f >> r >> c >> v;
        if(r > cr){
            while(cr < r) {
                row.push_back(values.size());
                ++cr;
            }
        }
        col.push_back(c);
        values.push_back(v);
    }
    return new SparseMatrix(n, values, col, row);
}

std::vector<double> loadVector(){
    cout << "Please enter your vector name" << endl;
    string s;
    cin >> s;
    ifstream f(s);
    if(!f.is_open()) {
        throw invalid_argument("Bad file" + s);
    }
    std::vector<double> v;
    size_t n;
    f >> n;
    double x;
    for(size_t j = 0; j < n; ++j) {
        f >> x;
        v.push_back(x);
    }
    return v;
}

void printResult(vector<double> &r) {
    char y = 0;
    while(y != 'y' && y !='n') {
        cout << ( y == 0 ? "" : "Please type y|n\n") << "Do you want to print result to the file? y/n" << endl;
        cin >> y;
    }
    if(y=='y') {
        while(true) {
            string s;
            cout << "Please type the name of the output file." << endl;
            cin >> s;
            ofstream out;
            out.open(s);
            if (!out.is_open()) {
                cout << "Bad file, type 'c' if you want to cancel output, another character if you want to retype the name of the file." << endl;
                char c;
                cin >> c;
                if(c=='c') {
                    break;
                }
            }
            else {
                out << r.size() << endl;
                for (long double a : r) {
                    out << a << endl;
                }
                out.close();
                break;
            }
        }
    }
    y = 0;
    while(y != 'y' && y !='n') {
        cout << ( y == 0 ? "" : "Please type y|n\n") << "Do you want to print result to the output? y/n" << endl;
        cin >> y;
    }
    if ( y == 'y') {
        for(auto a: r)
            cout << a << endl;
    }
}

int main(int argc, char** argv) {
    int opt;
    Method m = GRADIENT_DESCENT;
    char type = 'd';

    while ( (opt = getopt(argc, argv, "dcDC")) != -1 ) {  // for each option...
        switch ( opt ) {
            case 'c':
            case 'd':
                type = opt;
                break;
            case 'C':
                m = CONJUGATE_GRADIENT;
                break;
            case 'D':
                m = GRADIENT_DESCENT;
                break;
            case '?':
                cerr << "Unknown option: '" << char(optopt) << "'!" << endl;
                break;
            default:
                cerr << "Error getting configuration" << endl;
        }
    }
    Matrix * matrix;
    if(type == 'd')
        matrix = loadDenseMatrix();
    else
        matrix = loadSparseMatrix();
    vector<double> vec = loadVector();
    auto result = Solver::solve(m, matrix,vec);

    printResult(result);

    return 0;
}
