# NI-NON


Semestrální práce na předmět NI-NON. Veškeré kódy jsou v adrešáři src, v adresáři test jsou základní testy. Testovací data jsou v záložce data. 

Program je potřeba zkompilovat - je zde přiložen CMakeLists. Na vstupu je možné zadat přepínače `-cd` na compressed matici (`-c`) případně densed matici (`-d`, deafultní hodnota) a obdobně `-CD` pro volbu metody `-C` pro conjugate gradient, (`-D`) pro gradient descent. Vstup a výstup jsou ze souboru, interaktivně. 

